from pprint import pprint

import requests
import time, datetime


class CheckDate:
    _exchange_date = None

    def __init__(self, exchange_date: str):  # format "yyyymmdd"
        self.exchange_date = exchange_date

    @property
    def exchange_date(self):
        return self._exchange_date

    @exchange_date.setter
    def exchange_date(self, value):
        if not isinstance(value, str):
            raise SystemExit('exchange_date must be of string type')  # to compare dates with strptime
        elif len(value) != 8:
            raise SystemExit('Invalid date, you should use format "yyyymmdd"')

        try:
            time.strptime(value, "%Y%m%d")
        except ValueError:
            raise SystemExit('This date does not exist! You should use format "yyyymmdd"')
        else:
            current_date = datetime.datetime.today()  # ignore time zones to simplify
            chosen_date = datetime.datetime.strptime(value, "%Y%m%d")
            if current_date < chosen_date:
                raise SystemExit('This date has not yet come!')

        self._exchange_date = value


class CurrencyExchangeRates(CheckDate):

    def get_exchange_rates(self):
        """
        gets exchange rates from NBU website
        """
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={self.exchange_date}&json'
        try:
            response = requests.get(url, timeout=15)
        except requests.exceptions.ConnectionError:
            raise SystemExit('Check the internet connection')
        else:
            if 300 > response.status_code >= 200:
                if 'application/json' in response.headers.get('Content-Type', ''):
                    try:
                        data = response.json()
                    except Exception:
                        raise SystemExit('Something went wrong')
                    else:
                        if data:
                            exchange_rates = {}
                            for currency in data:
                                try:
                                    key, value = currency["txt"], currency["rate"]
                                except Exception:
                                    raise SystemExit('No information about rates')
                                else:
                                    exchange_rates[key] = value
                            return exchange_rates
                        else:
                            raise SystemExit('No rates for this date')
                else:
                    raise SystemExit('application/json was not found in Content-Type')
            else:
                raise SystemExit(f'Something went wrong, response status code is {response.status_code}')

    def write_exchange_rates(self):
        """
        writes rates to the file named as exchange date
        """
        exchange_rates = self.get_exchange_rates()
        file_name = datetime.datetime.strptime(self.exchange_date, "%Y%m%d").strftime("%d_%m_%Y")
        with open(f'{file_name}.txt', "w") as file:
            for key, value in exchange_rates.items():
                file.write(f'{key} to UAH: {value}\n')


rate = CurrencyExchangeRates('20130808')
pprint(rate.get_exchange_rates())
rate.write_exchange_rates()
