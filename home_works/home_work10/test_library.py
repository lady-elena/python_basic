from library import multiply_and_exponentiate
import pytest
import math


@pytest.mark.parametrize('multiplier1, multiplier2, exponent, expected', [(2, 3, 2, int), (4.1, 3, 2, float)])
def test_result_type(multiplier1, multiplier2, exponent, expected):
    assert type(multiply_and_exponentiate(multiplier1, multiplier2, exponent=2)) == expected, 'not expected type'


@pytest.mark.parametrize('multiplier1, multiplier2, exponent, expected', [(-2, 3, 2, 36), (4, 1, 2, 16)])
def test_result(multiplier1, multiplier2, exponent, expected):
    assert multiply_and_exponentiate(multiplier1, multiplier2, exponent=2) == expected, 'not expected result'


def test_value_error():
    with pytest.raises(ValueError):
        multiply_and_exponentiate(math.sqrt(-16), 1, exponent=-2)  # inappropriate value of argument, can't take the root of a negative number


def test_zero_division_error():
    with pytest.raises(ZeroDivisionError):
        multiply_and_exponentiate(0, 1, exponent=-2)  # zero can not be raised to a negative exponent


def test_type_error():
    with pytest.raises(TypeError):
        multiply_and_exponentiate('4', -1, exponent=3)  # inappropriate type of argument
