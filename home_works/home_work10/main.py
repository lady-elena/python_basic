from library import multiply_and_exponentiate


print(multiply_and_exponentiate(2, 3, exponent=2))
print(multiply_and_exponentiate(2, 1, exponent=-2))
print(multiply_and_exponentiate(-7, 1, exponent=2))
print(multiply_and_exponentiate(5, 3, exponent=3.1))
print(multiply_and_exponentiate(3.5, 3, exponent=3))
