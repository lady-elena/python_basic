# Напишіть гру "rock scissors paper lizard spock". Використайте розділення всієї програми на функції (
# Single-Responsibility principle). Як скелет-заготовку можна використати приклад з заняття.
# До кожної функції напишіть докстрінг або анотацію
import random

all_possible_figures = ["rock", "paper", "scissors", "lizard", "spock"]

combinations_to_win = {"rock": ["scissors", "lizard"],
                       "paper": ["rock", "spock"],
                       "scissors": ["lizard", "paper"],
                       "lizard": ["paper", "spock"],
                       "spock": ["rock", "scissors"]}


# This dict consists of winning combinations for player.
# Key is the strongest figure, values are figures that could be beaten by it


def player_turn(all_possible_figures):
    """
    This function gets players input
    and returns the figure that player has chosen.
    If the input is not in the list of all_possible_figures, it asks player
    to repeat choice
    Args:
        all_possible_figures: list with all figures which are used in the game
    Returns:
        (str):
        The figure that player has chosen
    """
    while True:
        player_figure = input("Choose the figure: ").lower()
        if player_figure in all_possible_figures:
            return player_figure
        else:
            print("You should choose Rock, Scissors, Paper, Lizard or Spock")


def computer_turn(all_possible_figures):
    """
    This function makes random choice of figure from the list of all_possible_figures
    and returns the result.
    Args:
        all_possible_figures: list with all figures which are used in the game
    Returns:
        (str):
         The figure that computer has chosen randomly
    """
    computer_figure = random.choice(all_possible_figures)
    return computer_figure


def who_win(player_figure, computer_figure, combinations_to_win):
    """
    This function compares player and computer figures to choose the winner and returns the result of the game
    Args:
        player_figure (str): The figure that player has chosen
        computer_figure (str): The figure that computer has chosen randomly
        combinations_to_win (dict): Winning combinations for player
        Key is the strongest figure, values are figures that could be beaten by it
    Returns:
        (str): message with the result of the game
    """
    if player_figure == computer_figure:
        return "Tie!"
    else:
        for key, value in combinations_to_win.items():
            if key == player_figure and computer_figure in value:
                return "Congrats, you have won!"
        else:
            return "You have lost"


def start():
    """
    This function starts the game
    """
    print("Let's play Rock Scissors Paper Lizard Spock")
    player_figure = player_turn(all_possible_figures)
    computer_figure = computer_turn(all_possible_figures)
    print("Computer chose:", computer_figure)
    print(who_win(player_figure, computer_figure, combinations_to_win))
    play_again()


def play_again():
    """
    This function proposes the player to play again.
    If he chooses "yes", it calls function that starts a new game
    """
    if input("Play again? (Yes/No) ").lower() == "yes":
        start()


start()
