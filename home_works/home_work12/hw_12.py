# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу int або float
# Доопрацюйте класс Line так, щоб в атрибути begin та end обʼєктів цього класу можна було записати тільки обʼєкти класу Point
# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point). Реалізуйте перевірку даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника. Для обчислень можна використати формулу Герона

class Point:
    x = None
    y = None

    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self.x = x
            self.y = y
        else:
            raise TypeError('parameters x and y must be of either int or float type')


class Line:
    begin = None
    end = None

    def __init__(self, begin, end):
        if isinstance(begin, Point) and isinstance(end, Point):
            self.begin = begin
            self.end = end
        else:
            raise TypeError('not the members of class Point')

    def length(self):
        """calculates the distance between start and end points"""
        res = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
        return res


class Triangle:
    def __init__(self, a, b, c):
        if isinstance(a, Point) and isinstance(b, Point) and isinstance(c, Point):
            self.a = a
            self.b = b
            self.c = c
        else:
            raise TypeError('not the members of class Point')

    def triangle_sides(self):
        """calculates the length of triangle sides"""
        ab = ((self.a.x - self.b.x) ** 2 + (self.a.y - self.b.y) ** 2) ** 0.5
        bc = ((self.b.x - self.c.x) ** 2 + (self.b.y - self.c.y) ** 2) ** 0.5
        ac = ((self.a.x - self.c.x) ** 2 + (self.b.y - self.c.y) ** 2) ** 0.5
        return ab, bc, ac

    def triangle_halfperimeter(self):
        """calculates the halfperimeter"""
        ab, bc, ac = self.triangle_sides()
        hp = (ab + bc + ac) / 2
        return hp

    def triangle_area(self):
        """calculates the area using Heron's formula"""
        ab, bc, ac = self.triangle_sides()
        hp = self.triangle_halfperimeter()
        area = (hp * (hp - ab) * (hp - bc) * (hp - ac)) ** 0.5
        return area


p1 = Point(1.1, 6)
p2 = Point(3, 4)
p3 = Point(11, 8)
my_line = Line(p1, p2)
my_triangle = Triangle(p1, p2, p3)
print('The length of the line is', my_line.length())
print('The area of the triangle is', my_triangle.triangle_area())
