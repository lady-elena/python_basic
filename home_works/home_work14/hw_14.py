# Візьміть свою гру з HW8 і перепишіть ії в обʼєктному стилі. Зробіть максимум взаємодії (як явної так і неявної) на рівні обʼєктів.
# Рекомендую подумати над такими класами як Player, GameFigure, Game. Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.


import random


class Figures:
    all_possible_figures = ["rock", "paper", "scissors", "lizard", "spock"]
    combinations_to_win = {"rock": ["scissors", "lizard"],
                           "paper": ["rock", "spock"],
                           "scissors": ["lizard", "paper"],
                           "lizard": ["paper", "spock"],
                           "spock": ["rock", "scissors"]}


class PlayerTurn(Figures):

    def get_human_figure(self):
        """
        Gets players input and returns the figure that player has chosen.
        If the input is not in the list of all_possible_figures, it asks player to repeat choice
        :return:
        The figure that player has chosen
        """
        while True:
            human_figure = input("Choose the figure: ").lower()
            if human_figure in self.all_possible_figures:
                print(f'Your figure is {human_figure}')
                return human_figure
            else:
                print("You should choose Rock, Scissors, Paper, Lizard or Spock")

    def get_computer_figure(self):
        """
        Makes random choice of the figure from the list of all_possible_figures
        :return:
        The figure that computer has chosen randomly
        """
        computer_figure = random.choice(self.all_possible_figures)
        print(f'Computer figure is {computer_figure}')
        return computer_figure


class Game(PlayerTurn):
    human_score = 0
    computer_score = 0

    def check_winner(self):
        """
        Compares player and computer figures to choose the winner
        :return:
        Result of the game
        """
        human_figure = self.get_human_figure()
        computer_figure = self.get_computer_figure()

        if human_figure == computer_figure:
            print("Tie!")
        else:
            for key, value in self.combinations_to_win.items():
                if key == human_figure and computer_figure in value:
                    self.human_score += 1
                    print("The winner is Human!")
                    print(f'The score is {self.human_score} : {self.computer_score}')
                    return "Human"
            else:
                self.computer_score += 1
                print("The winner is Computer")
                print(f'The score is {self.human_score} : {self.computer_score}')
                return "Computer"

    def play_again(self):
        """
        Proposes the Human to play again
        """
        if input("Play again? (Yes/No) ").lower() == "yes":
            self.game_start()

    def game_start(self):
        """Starts the game"""
        self.check_winner()
        self.play_again()


Game().game_start()
