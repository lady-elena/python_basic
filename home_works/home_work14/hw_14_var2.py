# Not sure that there is enough interaction between classes, so tried another variant (hw_14)

import random


class Game:
    score = {"Computer": 0, "Human": 0}

    def start(self):
        """
        Starts the game with the score 0:0
        """
        while True:
            game_result = Rules.check_winner(self, Player.get_human_figure(self), Player.get_computer_figure(self))
            if game_result == "Computer":
                Game.score["Computer"] += 1
            elif game_result == "Human":
                Game.score["Human"] += 1
            if input("Play again? (Yes/No) ").lower() == "no":
                break
        print(Game.score)


class Rules:
    all_possible_figures = ["rock", "paper", "scissors", "lizard", "spock"]

    combinations_to_win = {"rock": ["scissors", "lizard"],
                           "paper": ["rock", "spock"],
                           "scissors": ["lizard", "paper"],
                           "lizard": ["paper", "spock"],
                           "spock": ["rock", "scissors"]}

    def check_winner(self, human_figure, computer_figure):
        """
        Compares player and computer figures to choose the winner

        :param human_figure: The figure that player has chosen
        :param computer_figure: The figure that computer has chosen randomly
        :return:
        Result of the game
        """
        if human_figure == computer_figure:
            print("Tie!")
        else:
            for key, value in Rules.combinations_to_win.items():
                if key == human_figure and computer_figure in value:
                    print("The winner is Human!")
                    return "Human"
            else:
                print("The winner is Computer")
                return "Computer"


class Player:

    def get_human_figure(self):
        """
        Gets players input and returns the figure that player has chosen.
        If the input is not in the list of all_possible_figures, it asks player to repeat choice
        :return:
        The figure that player has chosen
        """
        while True:
            human_figure = input("Choose the figure: ").lower()
            if human_figure in Rules.all_possible_figures:
                return human_figure
            else:
                print("You should choose Rock, Scissors, Paper, Lizard or Spock")

    def get_computer_figure(self):
        """
        Makes random choice of the figure from the list of all_possible_figures
        :return:
        The figure that computer has chosen randomly
        """
        computer_turn = random.choice(Rules.all_possible_figures)
        print(f'Computer figure is {computer_turn}')
        return computer_turn


Game().start()
