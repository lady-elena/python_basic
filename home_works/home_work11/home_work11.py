# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд.
# Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

class Vehicle:
    age = None  # years
    colour = None


class Car(Vehicle):
    body = None
    engine_size = None  # liters


Tom_car = Car()
Tom_car.age = 5
Tom_car.colour = 'black'
Tom_car.body = 'sedan'
Tom_car.engine_size = 1.6

Pol_car = Car()
Pol_car.age = 1
Pol_car.colour = 'silver'
Pol_car.body = 'coupe'
Pol_car.engine_size = 2


class Plane(Vehicle):
    passenger_capacity = None  # pax
    cruising_speed = None  # km|h


airbus330_plane = Plane()
airbus330_plane.age = 5
airbus330_plane.colour = 'white'
airbus330_plane.passenger_capacity = 385
airbus330_plane.cruising_speed = 925

boeing737_plane = Plane()
boeing737_plane.age = 15
boeing737_plane.colour = 'blue'
boeing737_plane.passenger_capacity = 175
boeing737_plane.cruising_speed = 793


class Ship(Vehicle):
    displacement_tonnage = None  # tons
    draft = None  # meters


symphony_ship = Ship()
symphony_ship.age = 4
symphony_ship.colour = 'white'
symphony_ship.displacement_tonnage = 2220
symphony_ship.draft = 8

oceanbird_ship = Ship()
oceanbird_ship.age = 7
oceanbird_ship.colour = 'grey'
oceanbird_ship.displacement_tonnage = 2320
oceanbird_ship.draft = 7.95
