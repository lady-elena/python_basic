# завдання 1
# урл http://api.open-notify.org/astros.json
# Вивести список всіх астронавтів, що перебувають в даний момент на орбіті
# (дані не фейкові, оновлюються в режимі реального часу)
import requests

url = 'http://api.open-notify.org/astros.json'
response = requests.get(url)
try:
    data = response.json()
    try:
        people = data["people"]
        print("Astronauts are currently in space:")
        for astronaut in people:
            print(astronaut["name"])
    except Exception as e:
        print("An error has occurred")
except Exception as e:
    print("Check url or try later")

