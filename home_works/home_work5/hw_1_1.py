# завдання 1
# урл http://api.open-notify.org/astros.json
# Вивести список всіх астронавтів, що перебувають в даний момент на орбіті
# (дані не фейкові, оновлюються в режимі реального часу)
# I'm not sure that with a large number of names
# print in the loop will be completed quickly, so I made the second option with list
import requests
url = "http://api.open-notify.org/astros.json"
try:
    response = requests.get(url)
    data = response.json()
    people = data["people"]
    astronaut_names = []
    for astronaut in people:
        astronaut_names.append(astronaut["name"])
    print("Astronauts are currently in space:", *astronaut_names, sep='\n')
except:
    print("Sorry, something went wrong")
