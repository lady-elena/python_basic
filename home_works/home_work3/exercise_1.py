# 1. Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
# Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'". Слово та номер символу отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".
word = input("Write down your word here: ")
if word == "" or word.isspace():
    print("Do not see your word")
else:
    symbol_number = input("Which symbol you'd like to know?: ")
    if not symbol_number.isdecimal():
        print("Here must be only digits!")
    else:
        symbol_number = int(symbol_number)
        if symbol_number > len(word) or symbol_number == 0:
            print("Ooops, there is no symbol with that number")
        else:
            print(f'The {symbol_number} symbol in "{word}" is "{word[symbol_number - 1]}"')