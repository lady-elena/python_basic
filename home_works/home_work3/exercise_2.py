# 2. Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою).
# Напишіть код, який визначить кількість слів, в цих даних.
my_string = input("Write down your string here: ")
my_string_new = ""
if my_string == "" or my_string.isspace():
    print("Do not see your string")
else:
    for symbol in my_string:
        if symbol.isalnum() or symbol == "-" or symbol == "\'":
            my_string_new += symbol
        else:
            my_string_new += " "
    print("The number of words is", len(my_string_new.split()))

