#Задача 1: Створіть дві змінні first=10, second=30.
#Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
first = 10
second = 30
print("сума first та second =",first + second)

print("різниця first та second =",first - second)

print("first помножити на second =",first * second)

print("first у ступені second =",first ** second)

print("first поділене на second =",first / second)

print("ціла частина від ділення =",first // second)

print("залишок від ділення =",first % second)


#Задача 2: Створіть змінну і почергово запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.
compare_res = first < second
print("first менше second:",compare_res)

compare_res = first > second
print("first більше second:",compare_res)

compare_res = first == second
print("first дорівнює second:",compare_res)

compare_res = first != second
print("first не дорівнює second:",compare_res)

#Задача 3: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world".
# Виведіть на екран.
str1 = "Hello"
str2 = "world"
str3 = str1 + str2
print("str1 + str2 =",str3)