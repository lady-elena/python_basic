# 1. Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент,
# перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.
def get_float(arg1):
    try:
        return float(arg1)
    except Exception as e:
        return 0


print(get_float(input('Enter the argument: ')))
