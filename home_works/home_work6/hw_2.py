# 2. Напишіть функцію, що приймає два аргументи. Функція повинна
# якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# у будь-якому іншому випадку повернути кортеж з цих аргументів
def check_data_type(arg1, arg2):
    if (type(arg1) == int or type(arg1) == float) and (type(arg2) == int or type(arg2) == float):
        return arg1 * arg2
    elif type(arg1) == str and type(arg2) == str:
        return f'{arg1}{arg2}'
    else:
        return arg1, arg2


print(check_data_type(arg1="6", arg2="5"))
