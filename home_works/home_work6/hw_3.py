# 3. Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі",
# яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік.
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
def check_input():
    while True:
        try:
            age = int(input("Вкажiть ваш вiк: "))
        except Exception as e:
            print("Тут мають бути лише цифри")
            continue
        if 1 <= age <= 130:  # suppose that a person cannot be older than 130 years
            return age
        else:
            print("Це не схоже на реальний вiк")
            continue


def year_correct_form(age):
    age_last_digit = age % 10
    if age in [11, 12, 13, 14] or age_last_digit in [5, 6, 7, 8, 9, 0]:
        return "рокiв"
    if age_last_digit in [2, 3, 4]:
        return "роки"
    if age_last_digit == 1:
        return "рiк"


def message(age):
    if str(age).count("7") >= 1:
        return f'Вам {age} {year_correct_form(age)}, вам пощастить!'
    elif age < 7:
        return f'Тобі ж {age} {year_correct_form(age)}! Де твої батьки?'
    elif age < 16:
        return f'Тобі лише {age} {year_correct_form(age)}, а це фільм для дорослих!'
    elif age > 65:
        return f'Вам {age} {year_correct_form(age)}? Покажіть пенсійне посвідчення!'
    else:
        return f'Незважаючи на те, що вам {age} {year_correct_form(age)}, білетів всеодно нема!'


age = check_input()
print(message(age))
