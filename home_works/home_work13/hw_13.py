# Доопрацюйте всі перевірки на типи даних (x, y в Point, begin, end в Line, etc) - зробіть перевірки за допомогою property або класса-дескриптора.
# Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
# перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3

class Point:
    _x = None
    _y = None

    def __init__(self, x: int | float, y: int | float):
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise Exception('parameters x and y must be of either int or float type')

        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise Exception('parameters x and y must be of either int or float type')

        self._y = value


class Line:
    _begin = None
    _end = None

    def __init__(self, begin, end):
        self.begin = begin
        self.end = end

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, value):
        if not isinstance(value, Point):
            raise Exception('not the member of class Point')

        self._begin = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if not isinstance(value, Point):
            raise Exception('not the member of class Point')

        self._end = value

    def length(self):
        """calculates the distance between start and end points"""
        res = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
        return res


class Triangle:
    _a = None
    _b = None
    _c = None

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        if not isinstance(value, Point):
            raise Exception('not the member of class Point')

        self._a = value

    @property
    def b(self):
        return self._b

    @b.setter
    def b(self, value):
        if not isinstance(value, Point):
            raise Exception('not the member of class Point')

        self._b = value

    @property
    def c(self):
        return self._c

    @c.setter
    def c(self, value):
        if not isinstance(value, Point):
            raise Exception('not the member of class Point')

        self._c = value

    def calc_sides(self):
        """calculates the length of triangle sides"""
        ab = ((self.a.x - self.b.x) ** 2 + (self.a.y - self.b.y) ** 2) ** 0.5
        bc = ((self.b.x - self.c.x) ** 2 + (self.b.y - self.c.y) ** 2) ** 0.5
        ac = ((self.a.x - self.c.x) ** 2 + (self.b.y - self.c.y) ** 2) ** 0.5
        return ab, bc, ac

    def calc_halfperimeter(self):
        """calculates the halfperimeter"""
        ab, bc, ac = self.calc_sides()
        hp = (ab + bc + ac) / 2
        return hp

    def calc_area(self):
        """calculates the area using Heron's formula"""
        ab, bc, ac = self.calc_sides()
        hp = self.calc_halfperimeter()
        area = (hp * (hp - ab) * (hp - bc) * (hp - ac)) ** 0.5
        return area

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise Exception('not the member of class Triangle')
        return self.calc_area() == other.calc_area()

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            raise Exception('not the member of class Triangle')
        return self.calc_area() > other.calc_area()

    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            raise Exception('not the member of class Triangle')
        return self.calc_area() >= other.calc_area()

    def __str__(self):
        return f'({self.a.x},{self.a.y}--{self.b.x},{self.b.y}--{self.c.x},{self.c.y})'


p1 = Point(5, 6)
p2 = Point(13, 4)
p3 = Point(11, 8)
my_line = Line(p1, p2)
my_triangle1 = Triangle(p1, p2, p3)

p1 = Point(4, 9)
p2 = Point(3, 12)
p3 = Point(11, 12)
my_triangle2 = Triangle(p1, p2, p3)

print('The length of the line is', my_line.length())
print('The area of the first triangle is', my_triangle1.calc_area())
print('The area of the second triangle is', my_triangle2.calc_area())
print(f'{my_triangle1.calc_area()} = {my_triangle2.calc_area()} ------>', my_triangle1 == my_triangle2)
print(f'{my_triangle1.calc_area()} < {my_triangle2.calc_area()} ------>', my_triangle1 < my_triangle2)
print(f'{my_triangle1.calc_area()} >= {my_triangle2.calc_area()} ------>', my_triangle1 >= my_triangle2)
print('Coordinates of the first triangle vertices are', str(my_triangle1))
print('Coordinates of the second triangle vertices are', str(my_triangle2))
